package dawson;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {

    @Test
    public void echoReturnSameValue() {
        App app = new App();
        assertEquals("Testing Echo method", 5, app.echo(5));
    }

    @Test
    public void oneMoreReturnPlus1() {
        App app = new App();
        assertEquals("Testing oneMore method", 10, app.oneMore(9));
    }

}
